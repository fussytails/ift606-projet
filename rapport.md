                                  Introduction
                                  ============
    
    Depuis le début de la civilisation, des techniques sont utilisées pour 
    masquer des messages afin que seul des destinataires détenant la 
    connaissance pour déchiffrer celui-ci soient en mesure de connaître le 
    message original.
    
    Aujourd'hui encore, la cryptologie reste la meilleure option pour 
    s'assurer que seul un destinaire légitime puisse récupérer l'information 
    d'un message.

    Néanmoins, la cryptologie est une science composée de plusieurs méthodes 
    d'écriture de message secret. Chacune de ces méthodes ont leurs forces et 
    faiblesses déterminant les situations dans lesquels ces méthodes peuvent, 
    ou ne pas, être appropriées.
    
    Le nombre de facteurs à tenir compte dans le choix d'une méthode est 
    considérable et, il est impossible de tout prévoir les scénarios si on 
    laisse un message en liberté. C'est donc dans ce même ordre d'idée que 
    nous avons développer un système portable, combinant la cryptographie et le 
    concept de coffre-fort. Ce dernier permet d'éliminer le nombre de scénarios 
    imprévisible et lève d'un cran la sécurité d'un message "très" secret.

    Obectif:
    --------

    Un appareil pouvant contenir des données, sur lequel on peut y appliquer des 
    contraintes. Par exemple, et bien sur primordial, encrypter les données, 
    mettre un décompte sur la durée de vie des données, un nombre de tentatives 
    décryption, par la bande un nombre de succès ou d'échec, et possiblement 
    d'autres fonctionnalités. Ici nous avons voulu nous concentrer sur celles 
    mentionnées.


                                Fonctionnement:
                                ===============

    Tout d'abord, l'usager doit utiliser le programme python étant le client 
    permettant de communiquer avec l'appareil. Les options possibles sont de 
    choisir le mode d'encryption, un décompte pour la destruction automatique du 
    message, un nombre d'essaie infructueux avant destruction du message, et un 
    nombre de lecture avant destruction. Il y a aussi un mode 
    "mission_impossible" qui détruit le message à la première lecture. Pour 
    l'instant, seul l'algorithme RSA est implémenté comme choix de mode 
    d'encryption.

    Si l'appareil n'est pas vérouillé, l'usager peut fournir ou non une clé 
    publique suivit des données à encrypter si un clé est fournie. Dans le cas 
    où une clé est fournie, un prétraitement sera fait par le client mais 
    l'encryption même se fait dans l'appareil. Une fois les données dans 
    l'appareil, le client enverra un signal de vérrou à l'appareil. De cette 
    manière, nous avons un contrôle très strict sur les données qui seront 
    conservées. Dans le cas où il n'y a pas de clé, les données sont envoyés en 
    clair sur l'appareil.

    À partir de ce moment, lorsqu'un client voudra communniquer avec l'appareil 
    vérouillé, la seule option possible est de décrypter le message en 
    fournissant la clé privée.  Lorsque l'appareil reçoit la clé, il vérifie
    s'il s'agit de la bonne avec la décryption d'une sentinelle permettant de 
    valider celle-ci. Si la clé est valide, l'appareil décrypte les données et 
    retourne celles-ci au client.

    Matériel:
    ---------

    Pour implémenter notre preuve de concept, nous avons utilisé un circuit de
    prototypage Arduino(tm) UNO qui intègre un microcontroleur "Atmega328". 
    N'étant pas très puissant, nous avons dû composer avec les limitation. 
    Voici les spécifications de ce dernier :
        - Processeur    : AVR de 8 bits à 16 MHz
        - Mémoire flash : 32k de mémoire flash (2k pour le chargeur d'amorçage)
        - Mémoire vive  : 2k
        - EEPROM        : 1k

    Logiciel:
    ---------

    L'environnement de développement de la carte Arduino fournit des librairies 
    de bases C++ pour le processeur AVR. Dans cet environnement, les types 
    primitifs peuvent avoir des spécification différentes, par exemple le type 
    "int" est sur 16 bits et un "long" 32 bits ce dernier étant le plus grand 
    entier natif disponible. 
    
    Pour des clés d'encryption, les valeurs peuvent facilement dépacer ce qu'un
    "long" est capable de contenir, c'est pourquoi la librairie "BigNumber" a 
    été utilisée. Il s'agit d'une librairie "BCD" portée de "GNU/bc" vers AVR. 
    Cette librairie construit les nombres comme un tableau d'octets où chaque 
    octet représente un chiffre du nombre représenté.
    
    Pour communiquer avec l'arduino, un client, écrit en python, permet d'envoyer 
    des commandes et des données à l'appareil par port série. La librairie 
    python "Crypto" est utilisée pour dégager les informations du fichier "pem" 
    contenant les clés RSA et "PySerial" pour la connexion avec l'appareil 
    Arduino.

    Futur du projet:
    ----------------

    Une des issues possibles de ce projet pourrait être un appareil utilisant en 
    surface les protocoles standards d'accès à des disques faisant abstraction 
    au maximum des différents aspect technique montrés à la présentation. 
    Lorsqu'un système monterait celui-ci, il proposerait de fournir un 
    certificat où une clé privée. Une foncitonnalité essentielle serait 
    également l'impossibilité de pouvoir ouvrir l'appareil.  Si on essaierait 
    de l'ouvrir pour jouer au niveau du matériel, le contenu serait 
    automatiquement détruit.

    Implication du projet:
    ----------------------

    Bien que minimaliste, l'appareil était de budget raisonnable et englobait 
    toutes les fonctionnalités nous permettant de mettre sur pied l'idée 
    essentiel du projet, c'est-à-dire l'impossibilité de décrypter par force 
    brute hors ligne.
    
    Bien que l'encryption asymétrique est fort dur à décrypter, il est toujours 
    impossible de prévoir tout les scénarios, la technologie avance à grand pas, 
    à l'heure actuelle il est possible d'accéder à des unités de calcul très 
    puissant. Aussi, nous nous trouvons à l'aube des ordinateurs quantiques dont 
    on a toujours beaucoup de difficulté à entrevoir les possiblilités.


                                Travail effectué
                                ================

    Déroulement:
    ------------
    
    En résumé, le développement du projet s'est fait sur 3 semaines comme suit.
    
    1.  La genèse de l'idée est née des discussions et débats des idées ayant 
        préalablement été jetées sur la table. Des recherches on aussi été fait 
        pour évaluer la faisabilité des idées retenues. Une fois fixé, nous 
        avons fait une planification sommaire des prochaines étapes.

        Début des manipulations. Nous avons tenté d'apprivoiser, au mieux de nos 
        différentes contraintes, l'appareil Arduino. Ainsi, nous nous sommes 
        familiarisé avec l'environnement en C++, les librairies fournies et 
        abordé la communauté.

    2.  Début de l'implémentation. Nous avons choisi une approche pour 
        l'implémentation du client qui allait permettre la communication avec 
        l'appareil. Ensuite, avec une vision plus clair du fonctionnement, nous 
        avons entammé la programmation de l'arduino.
        
        Nous cherchions à nous concentrer sur le sujet principal de notre projet 
        et éviter les problématiques techniques. Ainsi, après avoir consulté la 
        communauté nous avons fait quelques test avec une librairie d'encryption 
        RSA pour les processeurs AVR. Les fonctionnalités offertes par la 
        librairie nous paraissaient parfaitement appropriées pour notre projet. 
        Finalement ce fut un échec et nous nous sommes reposé sur la librairie 
        "BigNumber" et implémenté les parties "allégées" de l'encryption nous 
        même.

    3.  Les derniers pas. Nous avons finaliser le client ainsi que le code de 
        l'appareil a peu de chose près. Nous avons mis plus de temps afin de 
        compléter le tout. Nous avons implémenter le prétraitement des clés et 
        des données dans le client, puis l'encryption même à l'intérieur du 
        arduino. Enfin nous avons finaliser les options que nous avons énumérer 
        pour l'appareil.


                                   Conclusion
                                   ==========

    Le projet a été très intéressant, nous avons pu explorer les possibilités 
    avec des appareils plus minimalistes. Le Arduino est une option très 
    intéressante, nous avons travailler avec différentes contraintes. Nous 
    aurions voulu ajouter plusieurs foncitonnalités et le projet laisse place 
    a encore beaucoup d'avancement.

