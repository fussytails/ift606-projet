#include "BigNumber.h"
#include <EEPROM.h>
#include <SimpleTimer.h>

bool locked;
int attempts, decryptions, timerId, timeId;
long time;
String msg, last_block;
BigNumber last_cipher;
SimpleTimer timer;

void setup() {
    Serial.begin(9600);
    locked = false;
    time = 0;
    attempts = -1;
    decryptions = -1;
    BigNumber::begin();
    Serial.setTimeout(10000);
}

void loop() {
    timer.run();
}
void destroy() {
    msg.remove(0);
}
void decTime() {
    --time;
}

String receive_string() {
    while(!Serial.available()) {}
    return Serial.readStringUntil('\0');
}

int get_key(BigNumber &key, BigNumber &n) {
    key = BigNumber(receive_string().c_str());
    String ns = receive_string();
    n = BigNumber(ns.c_str());
    return ns.length()-1;
}

void serialEvent(){
    unsigned char command = Serial.read();
    if (locked) {
        switch (command){
            case 8:
                {
                    BigNumber key,n;
                    int chunk_len = get_key(key,n);
                    BigNumber check1 = last_cipher.powMod(key, n);
                    BigNumber check2(last_block.c_str()); 
                    if (check1 == check2) {
                        for(int i=0; i<msg.length()-chunk_len; i+=chunk_len) {
                            Serial.println(BigNumber(msg.substring(i,i+chunk_len).c_str()).powMod(key,n));
                        }
                        Serial.println("00000000");
                        Serial.println(last_block);
                        if (--decryptions == 0)
                            destroy();
                    } else {
                        // decrease try counter
                        if (--attempts == 0)
                            destroy();
                    }
                }
                break;
            case 9:
                Serial.println(locked);
                Serial.println(msg.length());
                Serial.println(attempts);
                Serial.println(decryptions);
                Serial.println(time);
                break;
        }
    } else {
        switch (command){
            case 0://receive data, encrypt
                {
                    msg.remove(0);
                    BigNumber key,n;
                    int chunk_len = get_key(key,n);
                    while(true) {
                        char in_data[300] = "";
                        int  bytes_read;
                        if ((bytes_read = Serial.readBytesUntil('\0', in_data, chunk_len))){
                            in_data[bytes_read + 1] = '\0';
                            BigNumber block(in_data);
                            Serial.println(block);
                            block = block.powMod(key, n);
                            char * encrypted = block.toString();
                            Serial.println(block);
                            Serial.write(0);
                            msg += encrypted;    
                            free(encrypted);
                            if (bytes_read < chunk_len) {
                                last_block = in_data;
                                last_cipher = block;
                                break;
                            }
                            in_data[0] = '\0'; // erase the previous data
                        }
                    }
                }
                break;
            case 1://reveive data, no encryption
                msg.remove(0);
                msg = receive_string();
                break;
            case 2: //set timeout
                while(!Serial.available()) {}
                time = Serial.parseInt();
                timeId = timer.setInterval(1,decTime);
                timer.disable(timeId);
                timerId = timer.setTimeout(time,destroy);
                timer.disable(timerId);
                break;
            case 3:// successful decryption tries
                while(!Serial.available()) {}
                decryptions = Serial.read();
                break;
            case 4: // max decryption attempts
                while(!Serial.available()) {}
                attempts = Serial.read();
                break;
            case 5: // lock
                if (time != 0)
                    timer.enable(timeId);
                    timer.enable(timerId);
                locked = true;
                break;
            case 6: // get raw data
                if(msg.length() > 0)
                    Serial.println(msg);
                else
                    Serial.println("No data");
                break;
            case 9:
                Serial.println(locked);
                Serial.println(msg.length());
                Serial.println(attempts);
                Serial.println(decryptions);
                Serial.println(time);
                break;
        };
    }
}

