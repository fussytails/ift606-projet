#ifndef SECRAMFS_h
#define SECRAMFS_h

#include "EEPROM.h"
#include <inttypes.h>

class SecRamFSClass{
    private:
        // where to write?
        unsigned int end;
        unsigned int cursor;
    public:

        SecRamFSClass():pos{0},cursor{0}{};
        bool at_end(){cursor >= end};
        void writeBytes(int idx, int, len, uint8_t bytes[]);
        void appendBytes(int len, uint8_t bytes[])
        void readBytes(uint8_t buf[], int idx, int len);
        void readBytes(uint8_t buf[], int len);
        void resetCursor(){cursor = 0};
        


}
static SecRamFSClass SecRamFS;
#endif
