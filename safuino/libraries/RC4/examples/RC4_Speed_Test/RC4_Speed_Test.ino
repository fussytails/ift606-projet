//RC4 encrypt/decrypt test
//runs 100 times and displays time

#include <RC4.h>

RC4 test;

void setup()
{
  Serial.begin(9600);
  while (!Serial);
}

void loop()
{
  if (Serial.available()>1)
  {
  int garb = Serial.read();
  uint32_t start = millis();
  for(int i=0; i< 100; i++)
  {
    test.omni_crypt("SeedTest","Key");
  }
  uint32_t duration = millis() - start;

  Serial.print("100 decryptions: ");   
  Serial.print(duration/100.0, 2);   
  Serial.println(" msec per call");
  }
 }