//RC4 encrypt/decrypt test

#include <RC4.h>

RC4 test;

void setup()
{
  Serial.begin(9600);
  while (!Serial);
}

void loop()
{
  if (Serial.available()>1)
  {
  int garb = Serial.read();
    
    Serial.print("Encrypted: ");
    String enc = test.omni_crypt("A long string for testing!","My key");
    Serial.println(enc);
    Serial.print("Decrypted: ");
    Serial.println(test.omni_crypt(enc,"My key"));

  }
 }