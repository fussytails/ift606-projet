//RC4 encrypt/decrypt test
//type in text within the serial monitor
//to see encrypted hex
#include <RC4.h>

RC4 test;
String inData;

void setup()
{
  Serial.begin(9600);
  while (!Serial);
}

void loop()
{
  inData="";
  if (Serial.available()>0)
  {
    int h = Serial.available();
    
    for (int i=0;i<h;i++){
      inData += (char)Serial.read(); //grabs serial data
    }
    
    Serial.print("(hex)Encrypted: ");
    hex_print(test.omni_crypt(inData,"Key"));
    Serial.print("\n(char)Encrypted: ");
    Serial.println(test.omni_crypt(inData,"Key"));
    Serial.print("Decrypted: ");
    Serial.println(test.omni_crypt(test.omni_crypt(inData,"Key"),"Key")); //decrypt
  }
}

void hex_print(String temp) //display hex with leading zeros
{
  for(int i = 0; i < temp.length(); i++)
  {
    if((byte)temp[i] < 11)
      Serial.print("0");
    Serial.print((byte)temp[i],HEX);
    Serial.print(" ");
  }
}