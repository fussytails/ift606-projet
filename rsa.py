#!/usr/bin/env python
""" For Python3 """

import random as rand
from fractions import gcd
from math import log10, sqrt

def is_prime(num):
    """ check if "num" is a prime number """
    if not num or (num%2 == 0 and num > 2):
        return False
    return all(num % d for d in range(3, int(sqrt(num))+1, 2))

def find_prime(numlen): #num_min, num_max):
    """
    Find a random prime number with the length "numlen"

    Args:
        numlen (int): How many digits the number should have.

    Returns:
        int: a random prime number with the amount of digit specified in
             "numlen".
    """
    num_min = 10**(numlen-1)
    num_max = 10**numlen-1
    num = None
    while not is_prime(num):
        num = rand.randint(num_min, num_max)
    return num


def pick_e(phi_n, num_p, num_q):
    """
    Pick a random number where the GCD of the later and phi_n is 1.

    Args:
        phi_n (int) : Euler Phi number
        num_p (int) : First random prime number
        num_q (int) : Second random prime number

    """
    num_e = 0
    while not gcd(num_e, phi_n) == 1:
        num_e = rand.randint(max(num_p, num_q), phi_n)
    return num_e

#http://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu#L.27algorithme
def eeuclide(term_a, term_b):
    """
    Used to find the modular inverse of 2 number, it's the extended Euclidian
    algorithm.

    Args:
        term_a (int)
        term_b (int)
    Returns:
        The GCD, the modulus, the nothing
    """
    if term_a == 0:
        return (term_b, 0, 1)
    else:
        term_g, term_y, term_x = eeuclide(term_b % term_a, term_a)
    return (term_g, term_x - (term_b // term_a) * term_y, term_y)

def modinv(num_e, phi_n):
    """
    Return the modular inverse of [num_e⁻¹ (mod phi_n)]

    Args:
        num_e (int): The random number given by "pick_e()" function.
    Return
        int: the modular inverse of [num_e⁻¹ (mod phi_n)].
    """
    term_g, term_x, _ = eeuclide(num_e, phi_n)
    if term_g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return term_x % phi_n

def generer_cles(bits=96):
    """
    Génère les clées de chiffrements privée et publique.

    Args:
        bits (int): La taille des clées en bit.
    Returns:
        tuple : contenant les données de la clée publique (e, n)
        tuple : contenant les données de la clée privée (p, q, d, n)
    """
    numlen = bits/8
    num_p = find_prime(numlen) #p
    num_q = find_prime(numlen) #q
    while num_p == num_q:
        num_q = find_prime(numlen)

    num_n = num_p*num_q
    phi_n = (num_p-1)*(num_q-1)
    num_e = pick_e(phi_n, num_p, num_q)
    num_d = modinv(num_e, phi_n) # inverse de num_e mod(phi_n)
    pub = (num_e, num_n)
    priv = (num_p, num_q, num_d, num_n)
    return pub, priv

def get_as_ascii(message):
    """
    Retourne la chaîne de code ascii correspondant au caractères du message 
    d'entréé

    Args:
        message (string): le message à convertir.
    Returns:
        string: la chaîne de code ascii."""
    return "".join([str(ord(c)).zfill(3) for c in message])

def get_chunks(as_ascii, chunk_s):
    """
    Sépare une chaîne de caractères en blocs d'une taille déterminée.

    Args:
        as_ascii (string): La chaine à découper.
        chunk_s (int): La taille des blocs.
    Returns:
        list: Une liste contenant les blocs.
    """
    return [as_ascii[i:i+chunk_s] for i in range(0, len(as_ascii), chunk_s)]

def encode(chunks, pubk):
    """
    Calcule du chiffrement lui-même à partir d'une liste de blocs.

    Args:
        chunks (list): Liste de blocs.
        pubk (tuple): La clée publique.
    Returns:
        string: La string contenant le message (de départ) encrypté.
    """
    num_e, num_n = pubk
    as_ints = [int(s) for s in chunks]
    enc = [pow(i, num_e, num_n) for i in as_ints]
    as_str = " ".join([str(i) for i in enc])
    return as_str

def chiffrer(cle_public, message):
    """
        La méthode principale pour chiffrer des chaînes de caractères.

    Args:
        cle_public (tuple): La clée publique.
        message (string): La chaîne de caractère à encrypter.
    Returns:
        string: Le message encrypté.
    """
    _, num_n = cle_public
    as_ascii = get_as_ascii(message)
    chunk_s = int(log10(num_n))
    chunks = get_chunks(as_ascii, chunk_s)
    encoded = encode(chunks, cle_public)
    return encoded

def dechiffrer(cle_privee, message_chiffre):
    """
        Décrypte une chaîne de caractères préalablement chiffrée.

    Args:
        cle_privee (tuple): La clée privée.
        message_chiffre (string): La chaîne de caractère à déchiffrer.
    Returns:
        string: Le message décrypté
    """
    _, _, num_d, num_n = cle_privee
    chunk_s = int(log10(num_n))
    splitted = message_chiffre.split()
    as_ints = [int(s) for s in splitted]
    dec = [pow(i, num_d, num_n) for i in as_ints]
    chunks = [str(i).zfill(chunk_s) for i in dec]
    chunks.append(str(int(chunks.pop())))
    as_ascii = "".join(chunks)
    chars = [as_ascii[i:i+3] for i in range(0, len(as_ascii), 3)]
    message_chiffre = "".join([chr(int(c)) for c in chars])
    return message_chiffre

def test(message = "OpenSSL is Really Cool!!!"):
    """ just a test """
    print("Message to encrypt : %s" % message)
    pub, priv = generer_cles(32)
    enc = chiffrer(pub, message)
    dec = dechiffrer(priv, enc)
    print("Decrypted message : ", dec)

def tests():
    """ just many tests """
    test()
    test("Une autre phrase, celle là plus longue et avec \n des retours de \nchariot")
    test("Des ch@aractere fuckés")
