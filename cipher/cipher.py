from serial import Serial
from math import ceil
KEY    = b'\x00'
DATA   = b'\x01' 
DEC    = b'\x02'
RAW    = b'\x08'
STATUS = b'\x09'
EOL    = b'\x00'
OPT_TIMEOUT = b'\x05'
OPT_MODEMI  = b'\x06'
OPT_END_OF_TIME = b'\x07'
KEY_MAX_LENGTH  = 512 
class Arduino(object):
    def __init__(self, tty='/dev/ttyACM0', speed=9600, key=None):
        self.key = key
        self.io_file = None
        self.timeout = None
        self.mode_mi = None
        if self.connect(tty, speed):
            print('Safe is now connected and ready to use.')
            if self.key:
                if hasattr(self.key, 'd'):
                    print('A keypair of length %d bits has been sat' % self.key.size)
                else:
                    print('A public key of length %d has been sat.' % self.key.size)
    def connect(self, tty, speed):
        self.serial = Serial(tty, speed, timeout=2)
        return bool(self.serial)
    def close(self):
        self.send("".join([OPT_TIMEOUT,
                           self.timeout,
                           OPT_MODEMI,
                           self.mode_mi,
                           OPT_END_OF_TIME]))
        self.serial.close()
    def send(self, data):
        if self.serial.isOpen():
            if type(data) is not bytes:
                self.serial.write(bytes(data, encoding='utf-8'))
            else:
                self.serial.write(data)
            self.serial.flush()
            #if self.serial.read() == '\x10':
            #    print("Data digested")
    @property
    def key(self):
        return self._key
    @key.setter
    def key(self, key):
        if type(key) is str:
            key = Key(in_file=key)
        if key != None and key.size > KEY_MAX_LENGTH:
            raise Exception("Key too long %d bits, max is %d" % (key.size, KEY_MAX_LENGTH))
        self._key = key
    @property
    def response(self):
        if self.serial.isOpen():
            print("Getting response")
            res = self.serial.readline()
            return res
        return None
    @property
    def status(self):
        print("Asking status")
        self.send(STATUS)
        res = self.response.decode()
        #res = res.replace('\r\n', '')
        
        locked = bool(int(res[0]))
        data_len = int(res[1:])
        return locked, data_len
    def print_status(self):
        print(("Safe status\n"
               "    Safe lock          : %s\n"
               "    Space used (Bytes) : %s") % self.status)
    def send_key(self, key):
        data = "".join([str(key), EOL.decode()])
        print("Sending first part of the key : %s" % data)
        self.send(data)
        data = "".join([str(self.key.n), EOL.decode()])
        print("Sending second part of the key : %s" % data)
        self.send(data)

    def send_data(self, idata):
        self.serial.timeout = None
        if self.serial.isOpen():
            if self.key:
                max_to_send = len(str(self.key.n)) -1
                self.send(KEY)
                self.send_key(self.key.e)
                data = "".join([str(ord(c)).zfill(3) for c in idata])
                datat = [data[i:i+max_to_send] for i in range(0, len(data), max_to_send)]
                print("Sending chunks of data : %s" % idata)
                old_timeout = self.serial.timeout
                for d in datat:
                    #print("Current chunk : %s" % d)
                    print(d)
                    self.send("".join([d, EOL.decode()]))
                    print(self.serial.readline())
                    print(self.serial.readline())
                    self.serial.read()
                    if len(d) < max_to_send:
                        self.serial.timeout = old_timeout
                        #self.send(EOL));
                        break
            else:
                data = "".join([DATA.decode(), idata])
                print("""Storing (%s) """ % "".join([str(ord(c)).zfill(3) for c in idata]))
                self.send(data)
    def get_raw(self): ## for debug only
        self.send(RAW)
        return self.response
    def get_data(self, key=None):
        if self.serial.isOpen():
            if self.key:
                self.send(DEC)
                self.send_key(self.key.d)
                msg = str()
                while True:
                    chunk = self.serial.readline().decode().replace('\r\n','')
                    if chunk == "00000000":
                        chunk = self.serial.readline().decode().replace('\r\n','')
                        print(chunk)
                        msg += chunk
                        print('FIN')
                        break
                    chunk = chunk.zfill(len(str(self.key.n))-1)
                    print(chunk)
                    msg += chunk
                print(msg)
                return bytes([int(msg[i:i+3]) for i in range(0, len(msg), 3)]).decode()



                    


import codecs
import re
#from base64 import b64decode
import Crypto
from Crypto.PublicKey import RSA
import io
class Key(object):
    def __init__(self, in_file=None):
        self.pem_str = in_file
        self.rsakey = self.pem_str
        for e in self.rsakey.keydata:
            if hasattr(self.rsakey.key, e):
                setattr(self, e, getattr(self.rsakey.key, e))
            else:
                setattr(self, e, None)
    @property
    def rsakey(self):
        return self._rsakey
    @rsakey.setter
    def rsakey(self, pem_str):
        try:
            if type(pem_str) is str:
                self._rsakey = RSA.importKey(pem_str)
            elif type(pem_str) is Crypto.PublicKey.RSA._RSAobj:
                self._rsakey = pem_str
            else:
                self._rsakey = None
        except:
            print("pem_str, unsupported type, must be 'Crypto.PublicKey.RSA._RSAobj', a string containing valid pem data or None." % type(pem_str))
            self._rsakey = None

    @property
    def has_private(self):
        return self.rsakey.has_private() if self.rsakey else None
    @property
    def size(self):
        return self.rsakey.size()+1 if self.rsakey else None
    @property
    def pem_str(self):
        return self._pem_str
    @pem_str.setter
    def pem_str(self, in_file):
        try:
            if type(in_file) is str:
                print("opening the pem file")
                self._pem_str = codecs.open(in_file, 'rb', encoding='utf-8', errors='ignore').read()
            elif isinstance(in_file, io.IOBase):
                self._pem_str = in_file.read()
        except:
            print("pem_str, unsupported type, must be codecs oppened file, a filename or None")
            self._pem_str = None

