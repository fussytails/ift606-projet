
from serial import Serial
class Arduino(object):
    op_id_send_key  = chr(0)
    op_id_send_data = chr(1)
    def __init__(self, tty='/def/ttyACM0', speed=9600):
        self.connect(tty, speed)

    def connect(tty, speed):
        self.serial = Serial(tty, speed)

    def send_message(self, op_id):
        pass
    def send_data(self, data, key=None):
        if self.serial.isOpen():
            tout = ""
            if key:
                tout = "".join([op_id_send_key, key,])
            tout += "".join([op_id_send_data, data])
            self.serial.write(tout)
    def get_data(sefl, key=None):
        if self.serial.isOpen():
            if key:
                key = "".join([op_id_send_key, key,])
                self.serial.write(tout)
            tout = self.serial.readall()

import codecs
import re
import struct
from base64 import b64decode
from Crypto.PublicKey import RSA
class Key(object):
#    class _dertags(object):
#        SEQ      = 48
#        BIT_STR  =  3
#        INT      =  2
#        BYTE_STR =  4
#        NULL     =  5
#        OBJ_ID   =  6
#    _pem_sections = ["RSA PRIVATE KEY", "PUBLIC KEY"]
#    _pem_criteria = re.compile(u"-----BEGIN (%s)-----\n.+?\n-----END \\1-----\n" % "|".join(_pem_sections), re.DOTALL)
    def __init__(self, filename=None):
        self.pem_str = self._load_pem_str(filename) if filename else None
#        self.pem_parsed = self._parse(self.pem_str) if self.pem_str else None
        # TODO: decode that shit
#        key64 = self.pem_parsed.get('RSA PRIVATE KEY') or self.pem_parsed.get('PUBLIC KEY')
#        key64clean = re.sub('(-----.+-----|\n)', '', key64)
#        keyder = b64decode(key64clean)
        if self.pem_str:
            rsakey = RSA.importKey(self.pem_str)
            for e in rsakey:
                setattr(self, e, getattr(rsakey.key, e))
#        self.num_n = 'something' if self.pem_str else None
        # key is num_e for pubkey and num_d for private
        # num_d is mod inv
        # num_e is... idk
    def _load_pem_str(self, filename):
        pemfile = codecs.open(filename, 'rb', encoding='utf-8', errors='ignore')
        return pemfile.read()
#   def _parse(self, pem_str):
#       d = {match.group(1):match.group(0) for match in self._pem_criteria.finditer(pem_str)}
#       return d
#   def _decode(self, keyder):
#       length = keyder[1]
#       start_idx = 2
#       if length > 127:
#           start_idx += (length & 127)
#           blength = keyder[2:start_idx]
#           length = struct.unpack("!I", blength)[0]
#       payload = keyder[start_idx:start_idx+length]

#       decoded_seq = []
#       iidx = 0
#       while iidx < length:
#           tag = payload[iidx]
#           if tag == self._dertags.INT:
#               pass


        #for k, v in d.items():
        #    setattr(self, k, v)

pk = Key('keypairs.pem')
